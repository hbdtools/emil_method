import random

import pandas as pd
import numpy as np
import pyper as pr

INPUT_MATRIX = 'external_files/matrix_file.csv'

pop_matrix = pd.read_csv(INPUT_MATRIX, index_col=0, usecols=['AFR', 'ASN', 'EUR'])

NB_ITERATIONS = 10 
NB_SNPS_CHOSEN = 6
half_nb_snps_chosen = NB_SNPS_CHOSEN/2

r = pr.R(use_pandas=True)

for i in range(NB_ITERATIONS):

    random_snps = np.random.permutation(pop_matrix)
    snp_group_1, snp_group_2 = random_snps[:half_nb_snps_chosen], random_snps[-half_nb_snps_chosen:]

    r.assign("s1", snp_group_1)
    r.assign("s2", snp_group_2)
   
    print r['s1'], r['s2']
 
    r('result1 = princomp(s1)')
    r('result2 = princomp(s2)')
    
    print r['result1'], r['result2']




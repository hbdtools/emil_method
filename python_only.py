
import random

import pandas as pd
import numpy as np
from sklearn.decomposition import PCA

input_matrix = 'external_files/matrix_file.csv'


pop_matrix = pd.read_csv(input_matrix, index_col=0, usecols=['AFR', 'ASN', 'EUR'])

nb_iterations = 1
nb_snps_chosen = 10

def run_analysis():

    half_nb_snps_chosen = nb_snps_chosen/2

    for i in range(nb_iterations):

        random_snps = np.random.permutation(pop_matrix)
        snp_group_1, snp_group_2 = random_snps[:half_nb_snps_chosen], random_snps[-half_nb_snps_chosen:]

        pca1, pca2 = PCA(n_components=2), PCA(n_components=2)

        pca1.fit(snp_group_1)
        pca2.fit(snp_group_2)

        r1 = pca1.fit_transform(snp_group_1)
        r2 = pca2.fit_transform(snp_group_2)

        print 'group 1 explained variance', pca1.explained_variance_
        print 'group 2 explained variance', pca2.explained_variance_

        print 'loading per snp in group 1', r1
        print 'loading per snp in group 2', r2

    return pca1, pca2, r1, r2


if __name__ == '__main__':

    run_analysis()

"""
Todo: mean abs. loading, loadings for each SNP, size of the first factor, correlation between both factors
extracted
"""
